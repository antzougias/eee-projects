#!/usr/bin/env python3

# Copyright (c) 2020, University of Bristol <www.bristol.ac.uk>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice,
#    this list of  conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Authors:
#   George Oikonomou <g.oikonomou@bristol.ac.uk>
import os
import argparse
import logging
import sys
import openpyxl
from openpyxl.styles import Protection
from docx import Document
from docx import opc
import pprint
import time
import copy


class SupervisorSummaryDoc(object):
    def __init__(self, feedback_path, supervisor):
        self.supervisor = supervisor
        self.feedback_file = os.path.join(feedback_path, '%s.docx' % (self.supervisor,))
        logging.debug('Feedback form for "%s": "%s"' % (self.supervisor, self.feedback_file))

    def update(self, student_name, student_username, sections):
        logging.debug('Appending feedback for "%s" to "%s"' % (student_username, self.feedback_file))

        if EEEProjects.args.dry_run is False:
            try:
                document = Document(self.feedback_file)
                logging.debug('Found %s.' % (self.feedback_file,))
            except opc.exceptions.PackageNotFoundError:
                # It does not exist. Create it and add front matter
                document = Document()
                logging.debug('Creating %s.' % (self.feedback_file,))

                document.add_heading('Supervisor\'s Summary', 0)

                p = document.add_paragraph(style='Subtitle')
                p.add_run('Supervisor: %s' % (self.supervisor,))

            # Student info
            document.add_heading(' - '.join((student_name, student_username)), level=1)

            for section in sections:
                p = document.add_paragraph()
                p.add_run('%s:' % (section['title'],)).bold = True
                if section['list'] is True:
                    if section['content'] not in (None, []):
                        for f in section['content']:
                            document.add_paragraph(f, style='List Bullet')
                    else:
                        p.add_run(' None provided')
                else:
                    p.add_run(' %s' % (section['content'],))

            document.save(self.feedback_file)

        print('Wrote to "%s" for "%s"' % (self.supervisor, student_username))


class SummarySheet(object):
    def __init__(self, path, extra_columns):
        self.path = path
        self.wb = openpyxl.Workbook()
        self.ws = self.wb.active
        self.last_col = 'D'
        self.extra_columns = extra_columns

        # First, set column dimensions
        self.ws.column_dimensions['A'].width = 20
        self.ws.column_dimensions['B'].width = 20
        self.ws.column_dimensions['C'].width = 50
        self.ws.column_dimensions['D'].width = 20

        # Wide placeholder columns for feedback, staff notes etc
        for col in self.extra_columns:
            self.last_col = chr(ord(self.last_col) + 1)
            self.ws.column_dimensions[self.last_col].width = col['width']

        # Columns that we always expect to have
        columns = ['Student Username', 'Student Name', 'Programme', 'Supervisor']
        for extra_col in self.extra_columns:
            columns.append(extra_col['title'])

        self.ws.append(columns)

        # and style it
        ft = openpyxl.styles.Font(bold=True)
        al = openpyxl.styles.Alignment(horizontal='center')

        for cell in self.ws['1:1']:
            cell.font = ft
            cell.alignment = al

    def add_row(self, project):
        the_row = [project['student_username'], project['student_name'], project['programme'], project['supervisor']]

        for extra_col in self.extra_columns:
            try:
                if extra_col['sanitise']:
                    text = '\n'.join(project[extra_col['content']])
                else:
                    text = str(project[extra_col['content']])
                # In case one assessor has not returned the sheet yet and we only have content by 1/2 assessors
            except KeyError:
                text = str(extra_col['default_value'])

            the_row.append(text)

        self.ws.append(the_row)

    def finalise(self):
        # Turn on auto-filter
        self.ws.auto_filter.ref = 'A:' + self.last_col

        # Enable Text Wrapping and vertical alignment across the board
        for row in self.ws.iter_rows():
            for cell in row:
                al = copy.copy(cell.alignment)
                al.wrapText = True
                al.vertical = 'center'
                cell.alignment = al

    def save(self):
        self.wb.save(self.path)


class MarkerChecklist(object):
    def __init__(self, staff_path):
        self.staff_path = staff_path
        self.filename = os.path.join(self.staff_path, EEEProjects.constants['marking_checklist'])
        logging.debug('New checklist for "%s".' % (self.filename, ))

    def create(self):
        wb = openpyxl.Workbook()
        ws = wb.active

        # First, set column dimensions
        ws.column_dimensions['A'].width = 20
        ws.column_dimensions['B'].width = 10
        ws.column_dimensions['C'].width = 35
        ws.column_dimensions['D'].width = 70
        ws.column_dimensions['E'].width = 10
        ws.column_dimensions['F'].width = 20
        ws.column_dimensions['G'].width = 20

        # Create the first row
        ws.title = 'Marking Checklist'
        ws['A1'] = 'Student Name'
        ws['B1'] = 'Username'
        ws['C1'] = 'Programme'
        ws['D1'] = 'Project Theme/Area'
        ws['E1'] = 'Role'
        ws['F1'] = 'Admin Notes'
        ws['G1'] = 'Status (marker use)'

        # and style it
        ft = openpyxl.styles.Font(bold=True)
        al = openpyxl.styles.Alignment(horizontal='center')

        for cell in ws['1:1']:
            cell.font = ft
            cell.alignment = al

        ws.auto_filter.ref = "A:G"

        if EEEProjects.args.dry_run is False:
            wb.save(self.filename)

        logging.debug('Created new checklist "%s". Dry run: %s.' % (self.filename, EEEProjects.args.dry_run))

    def append(self, proj, marker_field):
        logging.debug('Updating checklist at "%s". Dry run: %s.' % (self.filename, EEEProjects.args.dry_run))

        if EEEProjects.args.dry_run is True:
            return

        wb = openpyxl.load_workbook(self.filename)
        ws = wb.active
        ws.append((proj.student_name, proj.student_user, proj.programme, proj.theme, marker_field, proj.admin_notes))
        wb.save(self.filename)


class EEEStaffList(object):
    def __init__(self):
        self.__staff_list__ = []

    def add_or_update(self, name, supervisor=0, assessor_1=0, assessor_2=0):
        if name == "":
            return

        staff_member = StaffMember(name)

        for s in self.__staff_list__:
            if s == staff_member:
                s.supervisor_load += supervisor
                s.assessor_1_load += assessor_1
                s.assessor_2_load += assessor_2
                logging.debug('Update {}: {}, {}, {}'.format(s.name, s.supervisor_load, s.assessor_1_load,
                                                             s.assessor_2_load))
                break

        else:
            staff_member.supervisor_load = supervisor
            staff_member.assessor_1_load = assessor_1
            staff_member.assessor_2_load = assessor_2
            self.__staff_list__.append(staff_member)
            logging.debug('Add {}: {}, {}, {}'.format(staff_member.name, staff_member.supervisor_load,
                                                      staff_member.assessor_1_load, staff_member.assessor_2_load))

    def sort(self):
        self.__staff_list__.sort(key=lambda staff_name: staff_name.name)

    def __str__(self):
        out = 'Staff list\n'
        for st in self.__staff_list__:
            out += '\t' + st.__str__() + '\n'

        # Remove the trailing newline
        return out[:-1]

    def __len__(self):
        return len(self.__staff_list__)


class EEEProjects(object):
    """This module's top-level class

    This is the top-level class of this module and primarily serves the following purposes:

    * Provides defaults for command line arguments and other configuration values

    * Parses command line arguments and stores them in a class attribute

    * Configures the logging module

    * Creates the output directory where marking forms will be saved

    * Parses project specification from a CSV file and stores them internally in the projects attribute

    Attributes:
        defaults: A dict with default values of command line parameters.
        args: Stores command line arguments.
        projects: A list of projects. Each one will be an instance of EEEProject
    """
    defaults = {
        'input_xlsx': 'projects.xlsx',
        'extract': False,
        'dry_run': False,
        'output_dir': os.path.join(os.getcwd(), 'output'),
        'input_extract_dir': os.path.join(os.getcwd(), 'forms'),
        'debug_level': 'ERROR',
        'mode': 'generate',
        'templates_dir': os.path.join(os.getcwd(), 'templates'),
    }

    constants = {
        'sub_dir_marks': 'for-blackboard-upload',
        'sub_dir_supervisor_summaries': 'for-supervisors',
        'sub_dir_full_info': 'full-info',
        'extracted_marks_csv': 'extracted-marks.csv',
        'extracted_feedback_csv': 'extracted-',
        'extracted_full_info': 'extracted-full-info.xlsx',
        'marking_checklist': '_marking-checklist.xlsx',
    }

    projects = []
    staff = EEEStaffList()

    @classmethod
    def __parse_args__(cls):
        debug_choices = ('DEBUG', 'INFO', 'WARNING', 'ERROR')
        mode_choices = ('generate', 'extract')
        stage_choices = ('ug-eee-interim', 'ug-eee-poster', 'ug-eee-thesis', 'ers-ipp', 'ers-rrr',
                         'pgt-poster', 'pgt-thesis')

        parser = argparse.ArgumentParser(add_help=False,
                                         description='This script operates in two modes. \
                                                      In the first mode, it reads project information from an excel \
                                                      file and it generates assessment forms based on a template. \
                                                      In the second mode, it reads a directory full of marking forms \
                                                      and automatically extracts marks and marker comments for later \
                                                      processing and/or Blackboard upload.')

        generate_group = parser.add_argument_group('Generate Forms')
        parser.add_argument('stage', nargs=1, action='store', choices=stage_choices,
                            help='Perform operations for the assessment stage of the unit of interest. For example, \
                                  to generate forms for PGT thesis assessment, mode must be pgt-thesis. This argument \
                                  is required.')
        generate_group.add_argument('-i', '--input-xlsx', action='store', default=EEEProjects.defaults['input_xlsx'],
                                    help='Read project specification from INPUT_XLSX. If this argument is omitted, \
                                          project specification will be read from %s.'
                                         % (EEEProjects.defaults['input_xlsx'],))
        generate_group.add_argument('-t', '--templates-dir', action='store',
                                    default=EEEProjects.defaults['templates_dir'],
                                    help='Pull form templates from TEMPLATES_DIR. If omitted, templates will be read \
                                          from "%s".'
                                          % (EEEProjects.defaults['templates_dir'],))

        extract_group = parser.add_argument_group('Extract Marks')
        extract_group.add_argument('-I', '--input-extract-dir', action='store',
                                   default=EEEProjects.defaults['input_extract_dir'],
                                   help='Read project specification from INPUT_EXTRACT_DIR. If this argument is \
                                         omitted, marks will be read from "%s".'
                                         % (EEEProjects.defaults['input_extract_dir'],))

        general_group = parser.add_argument_group('General Options')
        general_group.add_argument('-o', '--output-dir', action='store',
                                   default=EEEProjects.defaults['output_dir'],
                                   help='Save output files in OUTPUT_DIR. If omitted, files will be saved in "%s".'
                                         % (EEEProjects.defaults['output_dir'],))
        general_group.add_argument('-d', '--dry-run', action='store_true', default=EEEProjects.defaults['dry_run'],
                                   help='Dry run. Print what would be created, but do not create any actual files. \
                                         Default: %s' % (EEEProjects.defaults['dry_run']))
        general_group.add_argument('-D', '--debug-level', action='store', choices=debug_choices,
                                   default=EEEProjects.defaults['debug_level'],
                                   help='Log messages of severity DEBUG_LEVEL or higher (Default %s).'
                                         % (EEEProjects.defaults['debug_level'],))
        general_group.add_argument('-h', '--help', action='help',
                                   help='Show this message and exit')
        general_group.add_argument('-m', '--mode', action='store', choices=mode_choices,
                                   default=EEEProjects.defaults['mode'],
                                   help='Set the script in the desired MODE of operation (Default %s). \
                                         In mode "generate" the script generates marking forms for the correct stage \
                                         based on templates and an input list of projects. In "extract" mode, the \
                                         script extracts marks, student feedback and other marker comments from \
                                         marking forms and saves them in a format suitable for subsequent processing.'
                                         % (EEEProjects.defaults['mode'],))

        cls.args = parser.parse_args()

    @classmethod
    def __config_logging__(cls):
        # Configure logging
        logging.basicConfig(format='%(levelname)s:%(funcName)s:%(message)s', level=cls.args.debug_level)

    @classmethod
    def init(cls):
        """Class method that initialises the class itself

        This class method will parse command line arguments and store them in the args attribute.
        Default values for optional command line arguments are specified in the defaults class attribute.

        The class method will also initialise the logging module.
        """
        cls.__parse_args__()
        cls.__config_logging__()
        logging.debug(pprint.pformat(vars(cls.args), indent=2))
        logging.info('Initialised arguments and logging')

    @classmethod
    def __create_a_dir__(cls, target_dir):
        logging.debug('Creating dir "%s". Dry run: %s.' % (target_dir, cls.args.dry_run))

        if cls.args.dry_run is False:
            try:
                os.makedirs(target_dir)
            except FileExistsError:
                logging.error('"%s" already exists. Refusing to overwrite' % (target_dir,))
                sys.exit(1)

    @classmethod
    def create_out_dir(cls):
        """Creates the directory where forms / marks / feedback will be generated and saved"""
        # Always create the top-level output directory
        cls.__create_a_dir__(cls.args.output_dir)

        if EEEProjects.args.mode == 'extract':
            # and then all subdirs as required
            cls.__create_a_dir__(os.path.join(cls.args.output_dir, cls.constants['sub_dir_marks']))
            cls.__create_a_dir__(os.path.join(cls.args.output_dir, cls.constants['sub_dir_supervisor_summaries']))
            cls.__create_a_dir__(os.path.join(cls.args.output_dir, cls.constants['sub_dir_full_info']))

    @classmethod
    def parse_project_list(cls):
        """Collects project specifications from an input file"""
        logging.debug('Collecting projects from "%s"' % (cls.args.input_xlsx,))

        work_book = openpyxl.load_workbook(cls.args.input_xlsx)
        ws = work_book.active

        for row in ws.iter_rows(min_row=2, max_col=8, values_only=True):
            # This will put project information in a tuple
            # Create the project and add it to the list
            # Assessor 2 could be empty at this stage so we default to ""
            logging.debug('Processing next row %s' % (row,))

            # Workaround: Sometimes ws.iter_rows stops at the end of sheet content. Sometimes it continues and returns
            # and empty row. Add a break condition here where row[0] is None

            try:
                project = EEEProject(student_user=row[0], student_name=row[1], programme=row[2], theme=row[3],
                                     supervisor=row[4], assessor_1=row[5], assessor_2=row[6] or "",
                                     admin_notes=row[7] or "")
                cls.projects.append(project)
                print('Found project: %s' % (project,))

                # And for that project, add/update list of staff members
                cls.staff.add_or_update(project.supervisor, supervisor=1)
                cls.staff.add_or_update(project.assessor_1, assessor_1=1)
                cls.staff.add_or_update(project.assessor_2, assessor_2=1)
            except AttributeError as e:
                logging.debug('Read empty excel row. Break')
                break

        logging.info('Found %d projects and %d staff' % (len(cls.projects), len(cls.staff)))


class FormGenerator(object):
    def __init__(self, projects):
        self.pr_list = projects

    @staticmethod
    def __set_cell_value_from_name__(work_book, cell_name, value):
        cell_range = list(work_book.defined_names[cell_name].destinations)

        # We expect the range to be exactly 1 cell or there is a problem in the template
        sheet_name = cell_range[0][0]
        cell_address = cell_range[0][1].replace('$', '')

        # We have a cell reference now. Set the value
        work_sheet = work_book[sheet_name]
        work_sheet[cell_address].value = value

    @staticmethod
    def __unlock_merged_range__(work_book, range_name):
        # Opening a workbook and saving it again will break lock/unlock state of merged cells
        # Explicitly unlock all cells in a named range even when they are merged
        # Assume all cells in range are in the same sheet and that all cells in question are contiguous
        # So the range will look something like $A$32:$F$38
        cell_range = list(work_book.defined_names[range_name].destinations)

        work_sheet = work_book[cell_range[0][0]]
        cell_address = cell_range[0][1].replace('$', '')
        for row in range(0, len(work_sheet[cell_address])):
            for cell in work_sheet[cell_address][row]:
                cell.protection = Protection(locked=False)

    def __create_form__(self, template_name, form_path, proj, marker_field, unlock_ranges, write_supervisor,
                        write_role, role):
        logging.debug('"%s/%s":\n\tSU="%s"; SN="%s"; P="%s"\n\tT="%s"\n\tS="%s"; A1="%s"; A2="%s"'
                      % (os.path.basename(os.path.dirname(form_path)), os.path.basename(form_path),
                         proj.student_user, proj.student_name, proj.programme, proj.theme, proj.supervisor,
                         proj.assessor_1, proj.assessor_2))
        marker_name = getattr(proj, marker_field)
        work_book = openpyxl.load_workbook(template_name)
        self.__set_cell_value_from_name__(work_book, 'student_username', proj.student_user)
        self.__set_cell_value_from_name__(work_book, 'student_name', proj.student_name)
        self.__set_cell_value_from_name__(work_book, 'project_theme', proj.theme)
        self.__set_cell_value_from_name__(work_book, 'programme', proj.programme)

        # Sometimes the marker is the supervisor. In these cases, the supervisor appears in the "marker" range
        # and there is no need to write it again here.
        if write_supervisor:
            self.__set_cell_value_from_name__(work_book, 'supervisor', proj.supervisor)
        self.__set_cell_value_from_name__(work_book, 'marker', marker_name)

        if write_role:
            self.__set_cell_value_from_name__(work_book, 'marker_role', role)

        # Lock the sheet before saving. Allow formatting of cells and rows.
        # No password, just in case a user needs to modify something
        # False allows an action
        for work_sheet in work_book.worksheets:
            work_sheet.protection.formatRows = False
            work_sheet.protection.selectUnlockedCells = False

            work_sheet.protection.formatCells = True
            work_sheet.protection.selectLockedCells = True
            work_sheet.protection.objects = True
            work_sheet.protection.scenarios = True
            work_sheet.protection.formatColumns = True
            work_sheet.protection.insertColumns = True
            work_sheet.protection.insertRows = True
            work_sheet.protection.insertHyperlinks = True
            work_sheet.protection.deleteColumns = True
            work_sheet.protection.deleteRows = True
            work_sheet.protection.selectLockedCells = True
            work_sheet.protection.sort = True
            work_sheet.protection.autoFilter = True
            work_sheet.protection.pivotTables = True

            # Workaround: Unlock merged named ranges
            for unlock_range in unlock_ranges:
                self.__unlock_merged_range__(work_book, unlock_range)

            work_sheet.protection.enable()

        if EEEProjects.args.dry_run is False:
            work_book.save(form_path)

        print('Generated form for marker "%s", student "%s"-"%s"' % (marker_name, proj.student_user, proj.student_name))

    def generate(self):
        form_generators = {
            'ug-eee-interim': (
                {'base_filename': 'ug-eee-interim', 'field': 'assessor_1', 'write_supervisor': True,
                 'write_role': False,
                 'unlock_ranges': ('top_matter', 'feedback')},
            ),
            'ug-eee-poster': (
                {'base_filename': 'ug-eee-poster', 'field': 'assessor_1', 'write_supervisor': True,
                 'write_role': True,
                 'unlock_ranges': ('top_matter', 'feedback')},
                {'base_filename': 'ug-eee-poster', 'field': 'assessor_2', 'write_supervisor': True,
                 'write_role': True,
                 'unlock_ranges': ('top_matter', 'feedback')},
            ),
            'ug-eee-thesis': (
                {'base_filename': 'ug-eee-thesis-supervisor', 'field': 'supervisor', 'write_supervisor': True,
                 'write_role': True,
                 'unlock_ranges': ('top_matter', 'staff_notes_thesis', 'staff_notes_performance',
                                   'staff_notes_plagiarism',)},
                {'base_filename': 'ug-eee-thesis-assessor', 'field': 'assessor_1', 'write_supervisor': True,
                 'write_role': True,
                 'unlock_ranges': ('top_matter', 'staff_notes_plagiarism', 'staff_notes',)},
            ),
            'ers-ipp': (
                {'base_filename': 'ers-ipp', 'field': 'assessor_1', 'write_supervisor': True, 'write_role': False,
                 'unlock_ranges': ('top_matter', 'feedback')},
            ),
            'ers-rrr': (
                {'base_filename': 'ers-rrr', 'field': 'supervisor', 'write_supervisor': True, 'write_role': False,
                 'unlock_ranges': ('top_matter', 'feedback', 'staff_notes')},
            ),
            'pgt-poster': (
                # ToDo: Validate configuration using PGT Thesis as a template
                {'base_filename': 'pgt-poster', 'field': 'assessor_1', 'write_supervisor': True, 'write_role': True,
                 'unlock_ranges': ('top_matter', 'feedback',)},
                {'base_filename': 'pgt-poster', 'field': 'supervisor', 'write_supervisor': True, 'write_role': True,
                 'unlock_ranges': ('top_matter', 'feedback',)},
            ),
            'pgt-thesis': (
                {'base_filename': 'pgt-thesis-assessor', 'field': 'assessor_1', 'write_supervisor': True,
                 'write_role': False,
                 'unlock_ranges': ('top_matter', 'staff_notes', 'staff_notes_plagiarism',)},
                {'base_filename': 'pgt-thesis-supervisor', 'field': 'supervisor', 'write_supervisor': True,
                 'write_role': False,
                 'unlock_ranges': ('project_title_marker', 'staff_notes_thesis', 'staff_notes_performance',
                                   'staff_notes_plagiarism',)},
            ),
        }

        marker_field_mappings = {
            'supervisor': 'Supervisor',
            'assessor_1': 'Assessor 1',
            'assessor_2': 'Assessor 2',
        }

        logging.debug('Creating forms in mode "%s". Dry run: %s.' % (EEEProjects.args.stage[0],
                                                                     EEEProjects.args.dry_run))

        for form in form_generators[EEEProjects.args.stage[0]]:
            logging.debug('Will create "%s" for "%s"' % (form['base_filename'], form['field']))

        for pr in self.pr_list:
            for form in form_generators[EEEProjects.args.stage[0]]:
                # First, try to create the output dir for the supervisor/assessor
                staff_member = StaffMember(getattr(pr, form['field']))
                staff_member.create_dir_and_checklist()

                # Then, figure out the actual form's name
                template_name = os.path.join(EEEProjects.args.templates_dir,
                                             '%s-template.xlsx' % (form['base_filename'],))
                form_name = os.path.join(EEEProjects.args.output_dir, staff_member.name,
                                         '%s-%s-%s.xlsx' % (form['base_filename'], pr.student_user,
                                                            getattr(pr, form['field'])))

                # Add project information to this marker's checklist
                staff_member.marker_checklist.append(pr, marker_field_mappings[form['field']])

                # Lastly, create the form
                self.__create_form__(template_name, form_name, pr, form['field'], form['unlock_ranges'],
                                     form['write_supervisor'], form['write_role'], marker_field_mappings[form['field']])
                logging.debug('Written "%s" from "%s". Dry run: %s.' % (os.path.basename(form_name),
                                                                        os.path.basename(template_name),
                                                                        EEEProjects.args.dry_run))

        return


class EEEProject(object):
    def __init__(self, student_user='', student_name='', programme='', theme='', supervisor='', assessor_1='',
                 assessor_2='', admin_notes=''):
        self.student_user = student_user.strip()
        self.student_name = student_name.strip()
        self.programme = programme.strip()
        self.theme = theme.strip()
        self.supervisor = supervisor.strip()
        self.assessor_1 = assessor_1.strip()
        self.assessor_2 = assessor_2.strip()
        self.admin_notes = admin_notes

    def __str__(self):
        out = 'Username: "{}"; Student name: "{}"; Theme: "{}"; Supervisor: "{}"; Assessor 1: "{}"; ' \
              'Assessor 2: "{}"; Admin notes: "{}"' \
            .format(self.student_user, self.student_name, self.theme, self.supervisor, self.assessor_1, self.assessor_2,
                    self.admin_notes)
        return out


class StaffMember(object):
    def __init__(self, name):
        self.name = name
        self.supervisor_load = 0
        self.assessor_1_load = 0
        self.assessor_2_load = 0
        self.staff_path = os.path.join(EEEProjects.args.output_dir, self.name)
        self.marker_checklist = MarkerChecklist(self.staff_path)

    def __eq__(self, other):
        return self.name == other.name

    def __repr__(self):
        return repr((self.name,))

    def __str__(self):
        out = '{}: Supervisor={}, Assessor 1={}, Assessor 2={}'.format(self.name, self.supervisor_load,
                                                                       self.assessor_1_load, self.assessor_2_load)
        return out

    def create_dir_and_checklist(self):
        logging.debug('Creating "%s". Dry run: %s.' % (self.staff_path, EEEProjects.args.dry_run))

        if EEEProjects.args.dry_run is False:
            try:
                os.mkdir(self.staff_path)
            except FileExistsError:
                logging.debug('Output directory "%s" exists. This is normal, continuing.' % (self.staff_path,))
                return

            # If we did not return, add the checklist for this staff member
            self.marker_checklist.create()


class MarkExtractor(object):
    def __init__(self):
        self.files_list = []
        self.extracted_data = []

    def __add_or_update__(self, project):
        logging.debug(project)

        for proj in self.extracted_data:
            # We found a previously extracted match. Update in-place
            if proj['student_username'] == project['student_username']:
                logging.debug('Already extracted data for "%s". Update.' % (project['student_username'],))
                proj.update(project)
                break
        else:
            logging.debug('Create new extracted data for "%s"' % (project['student_username'],))
            self.extracted_data.append(project)

    def __scan_input_dir__(self):
        # find all xlsx files in form directory
        logging.debug('Scanning: "%s"' % (EEEProjects.args.input_extract_dir,))
        for root, directories, files in os.walk(EEEProjects.args.input_extract_dir):
            for file_name in files:
                if (os.path.splitext(file_name)[-1] == '.xlsx' and
                        os.path.splitext(file_name)[0][0] != '~' and
                        os.path.splitext(file_name)[0][0] != '_'):
                    file_path = os.path.join(root, file_name)
                    self.files_list.append(file_path)
                    logging.debug('Found "%s"' % (file_name,))

    def __extract_one__(self, work_book, named_range):
        cell_range = work_book.defined_names[named_range].destinations

        for sheet, coord in cell_range:
            # This is annoying. work_book[sheet][coord] can be:
            # An object of class openpyxl.cell or
            # A tuple of rows, with each row being a tuple of objects of class openpyxl.cell...
            # Here we make sure it's always the latter
            try:
                # If we already have a tuple this will be happy
                cells = tuple(work_book[sheet][coord])
            except TypeError:
                # and this puts the openpyxl.cell object inside a tuple
                cells = ((work_book[sheet][coord],),)

        # Now cells is definitely a nested tuple
        rv = []
        for row in cells:
            for cell in row:
                if cell.value is not None:
                    rv.append(str(cell.value))

        # If the named range list only has 1 element, return the element, else the list
        if len(rv) == 1:
            return rv[0]
        else:
            return rv

    def __extract_named_ranges__(self, file_name, named_ranges_append):
        # Always extract those, all forms, all stages
        named_ranges = [
            {'range_name': 'student_username', 'sanitise': False, 'default_value': ''},
            {'range_name': 'student_name', 'sanitise': False, 'default_value': ''},
            {'range_name': 'programme', 'sanitise': False, 'default_value': ''},
            {'range_name': 'project_theme', 'sanitise': False, 'default_value': ''},
            {'range_name': 'project_title', 'sanitise': False, 'default_value': 'Not specified on form'},
            {'range_name': 'supervisor', 'sanitise': False, 'default_value': ''}
        ]

        print('Extracting from "%s"' % (file_name,))

        work_book = openpyxl.load_workbook(file_name, read_only=True, data_only=True)

        # First extract the marker role
        role = self.__extract_one__(work_book, 'marker_role')
        project = {'marker_role': role}

        logging.debug('Role: "%s"' % (project['marker_role'],))

        # Extend based on the extraction descriptor and marker role
        logging.debug(named_ranges_append[role])
        named_ranges.extend(named_ranges_append[role])

        for named_range in named_ranges:
            extracted_range = self.__extract_one__(work_book, named_range['range_name'])

            if extracted_range == []:
                extracted_range = named_range['default_value']

            # Some named ranges need sanitisation.
            # When they get extracted they can be a list, or they can be a string. Make sure we have a list
            if named_range['sanitise']:
                if type(extracted_range) is not list:
                    extracted_range = [extracted_range]

            # Use custom name in the data structure if specified
            field = 'content'
            try:
                project[named_range[field]] = extracted_range
            except KeyError:
                field = 'range_name'
                project[named_range[field]] = extracted_range
            logging.debug('Extracted "%s" ("%s")="%s". Sanitise: %s' % (named_range['range_name'], named_range[field],
                                                                        project[named_range[field]],
                                                                        named_range['sanitise']))

        self.__add_or_update__(project)

    def __create_summary_sheet__(self, extra_columns):
        print('Creating file with all info: "%s/%s/%s"' % (EEEProjects.args.output_dir,
                                                           EEEProjects.constants['sub_dir_full_info'],
                                                           EEEProjects.constants['extracted_full_info']))
        full_info_path = os.path.join(EEEProjects.args.output_dir, EEEProjects.constants['sub_dir_full_info'],
                                      EEEProjects.constants['extracted_full_info'])

        sheet = SummarySheet(full_info_path, extra_columns)

        for project in self.extracted_data:
            sheet.add_row(project)

        sheet.finalise()

        if EEEProjects.args.dry_run is False:
            sheet.save()

        logging.debug('Saved feedback file "%s". Dry run: %s.' % (full_info_path, EEEProjects.args.dry_run))

    def __dump_marks_and_feedback_to_csv__(self, feedback_columns, mark_columns):
        # output to CSV file
        logging.debug('Creating files for BB upload in directory: "%s/%s"'
                      % (EEEProjects.args.output_dir, EEEProjects.constants['sub_dir_marks']))
        marks_path = os.path.join(EEEProjects.args.output_dir, EEEProjects.constants['sub_dir_marks'],
                                  EEEProjects.constants['extracted_marks_csv'])
        feedback_paths = [
            {
                'path': os.path.join(
                    EEEProjects.args.output_dir,
                    EEEProjects.constants['sub_dir_marks'],
                    EEEProjects.constants['extracted_feedback_csv'] + x['content'] + '.csv'),
                'content': x['content'],
                'column_name': x['column_name'],
                'default_value': x['default_value'],
            }
            for x in feedback_columns
        ]

        if EEEProjects.args.dry_run is False:
            marks_file = open(marks_path, 'w')
            marks_file.write('"Username";' + ';'.join(['"' + x['column_name'] + '"' for x in mark_columns]) + '\n')

            for path in feedback_paths:
                path['file'] = open(path['path'], 'w')

                path['file'].write(';'.join(('"Username"',
                                              '"%s"' % (path['column_name'],),
                                              '"Marking Notes"', '"Notes Format"',
                                              '"Feedback to Learner"', '"Feedback Format"')) + '\n')

            for project in self.extracted_data:
                output_row = '"%s"' % (project['student_username'],)
                for column in mark_columns:
                    try:
                        output_row = output_row + ';"' + str(project[column['content']]) + '"'
                    except KeyError:
                        output_row = output_row + ';"' + str(column['default_value']) + '"'

                output_row = output_row + '\n'
                marks_file.write(output_row)
                print('Wrote output for "%s": row="%s"' % (project['student_username'], output_row.rstrip()))

                for path in feedback_paths:
                    feedback = ''

                    try:
                        feedback_raw = project[path['content']]
                    except KeyError:
                        feedback_raw = path['default_value']

                    for f in feedback_raw:
                        # Sanitise: BB Upload fails if double quotes exist in the feedback string.
                        # Replace with single quotes.
                        f_quote_free = f.replace('"', "'")
                        # Sanitise: Line breaks within f need to become <br />
                        f_sanitised = f_quote_free.replace('\n', '<br />')
                        feedback = feedback + '<p>' + f_sanitised + '</p>\n'

                    feedback = feedback.rstrip()
                    path['file'].write('"%s";"Available";;"HTML";"%s";"HTML"\n' %
                                       (project['student_username'], feedback))

                    logging.debug('Wrote to "%s" for "%s"' % (path['path'], project['student_username']))

            marks_file.close()
            for path in feedback_paths:
                path['file'].close()

    def __create_supervisor_summary__(self, extractor_sections):
        logging.debug('Creating supervisor summary doc in directory: "%s/%s"'
                      % (EEEProjects.args.output_dir, EEEProjects.constants['sub_dir_supervisor_summaries']))
        feedback_path = os.path.join(EEEProjects.args.output_dir,
                                     EEEProjects.constants['sub_dir_supervisor_summaries'])

        for project in self.extracted_data:
            form = SupervisorSummaryDoc(feedback_path, project['supervisor'])

            # For each section in sections, extract the respective content from the project dict
            sections = []
            for section in extractor_sections:
                try:
                    sections.append({
                        'title': section['title'],
                        'content': project[section['content']],
                        'list': section['list'],
                    })
                # In case one assessor has not returned the sheet yet and we only have content by 1/2 assessors
                except KeyError:
                    sections.append({
                        'title': section['title'],
                        'content': section['default_value'],
                        'list': section['list'],
                    })

            form.update(project['student_name'], project['student_username'], sections)

    def extract(self):
        extractors = {
            'ug-eee-interim': {
                'mark_columns':
                    (
                        {'column_name': 'UG interim mark',
                         'content': 'mark_final',
                         'default_value': 0},
                    ),
                'feedback_columns':
                    (
                        {'column_name': 'UG interim mark', 'content': 'a1_feedback',
                         'default_value': ''},
                    ),
                'extract_named_ranges':
                    {
                        'Assessor 1': (
                            {'range_name': 'mark_final', 'content': 'mark_final', 'sanitise': False,
                             'default_value': 0},
                            {'range_name': 'marker', 'content': 'a1_name', 'sanitise': False, 'default_value': ''},
                            {'range_name': 'feedback', 'content': 'a1_feedback', 'sanitise': True,
                             'default_value': []},
                        ),
                    },
                'supervisor_summary_sections':
                    (
                        {'title': 'Project Theme', 'content': 'project_theme', 'list': False},
                        {'title': 'Project Title', 'content': 'project_title', 'list': False},
                        {'title': 'Assessor', 'content': 'a1_name', 'list': False},
                        {'title': 'Mark', 'content': 'mark_final', 'list': False},
                        {'title': 'Feedback', 'content': 'a1_feedback', 'list': True,
                         'default_value': ['Not returned yet']},
                    ),
                'summary_sheet_columns':
                    (
                        {'title': 'Assessor', 'content': 'a1_name', 'sanitise': False, 'width': 20,
                         'default_value': 'Not returned yet'},
                        {'title': 'Mark', 'content': 'mark_final', 'sanitise': False, 'width': 10,
                         'default_value': 'NULL', 'round': True},
                        {'title': 'Feedback', 'content': 'a1_feedback', 'sanitise': True, 'width': 100,
                         'default_value': 'Not returned yet'},
                    )
            },
            'ug-eee-poster': {
                'mark_columns':
                    (
                        {'column_name': 'A1 Poster [Total Pts: 100 Score] |724313', 'content': 'a1_mark',
                         'default_value': 0},
                        {'column_name': 'A2 Poster [Total Pts: 100 Score] |724314', 'content': 'a2_mark',
                         'default_value': 0},
                    ),
                'feedback_columns':
                    (
                        {'column_name': 'A1 Poster Feedback [Total Pts: 0 Text] |724315', 'content': 'a1_feedback',
                         'default_value': ''},
                        {'column_name': 'A2 Poster Feedback [Total Pts: 0 Text] |724316',
                         'content': 'a2_feedback',
                         'default_value': ''},
                    ),
                'extract_named_ranges':
                    {
                        'Assessor 1': (
                            {'range_name': 'mark_final', 'content': 'a1_mark', 'sanitise': False,
                             'default_value': 0},
                            {'range_name': 'marker', 'content': 'a1_name', 'sanitise': False, 'default_value': ''},
                            {'range_name': 'feedback', 'content': 'a1_feedback', 'sanitise': True,
                             'default_value': []},
                        ),
                        'Assessor 2': (
                            {'range_name': 'mark_final', 'content': 'a2_mark', 'sanitise': False,
                             'default_value': 0},
                            {'range_name': 'marker', 'content': 'a2_name', 'sanitise': False,
                             'default_value': ''},
                            {'range_name': 'feedback', 'content': 'a2_feedback', 'sanitise': True,
                             'default_value': []},
                        ),
                    },
                'supervisor_summary_sections':
                    (
                        {'title': 'Project Title', 'content': 'project_title', 'list': False},
                        {'title': 'A1 Feedback', 'content': 'a1_feedback', 'list': True,
                         'default_value': ['Not returned yet']},
                        {'title': 'A2 Feedback', 'content': 'a2_feedback', 'list': True,
                         'default_value': ['Not returned yet']},
                    ),
                'summary_sheet_columns':
                    (
                        {'title': 'Assessor 1', 'content': 'a1_name', 'sanitise': False, 'width': 20,
                         'default_value': 'Not returned yet'},
                        {'title': 'Assessor 2', 'content': 'a2_name', 'sanitise': False, 'width': 20,
                         'default_value': 'Not returned yet'},
                        {'title': 'A1 Poster', 'content': 'a1_mark', 'sanitise': False, 'width': 10,
                         'default_value': 'NULL'},
                        {'title': 'A2 Poster', 'content': 'a2_mark', 'sanitise': False, 'width': 10,
                         'default_value': 'NULL'},
                        {'title': 'A1 Feedback', 'content': 'a1_feedback', 'sanitise': True, 'width': 100,
                         'default_value': 'Not returned yet'},
                        {'title': 'A2 Feedback', 'content': 'a2_feedback', 'sanitise': True, 'width': 100,
                         'default_value': 'Not returned yet'},
                    )
            },
            'ug-eee-thesis': {
                'mark_columns':
                    (
                        {'column_name': 'Supervisor Performance', 'content': 'mark_performance',
                         'default_value': 0},
                        {'column_name': 'Supervisor Thesis', 'content': 'mark_thesis',
                         'default_value': 0},
                        {'column_name': 'Assessor Thesis', 'content': 'a1_mark',
                         'default_value': 0},
                    ),
                'feedback_columns': (),
                'extract_named_ranges':
                    {
                        'Assessor 1': (
                            {'range_name': 'mark_final', 'content': 'a1_mark', 'sanitise': False, 'default_value': 0},
                            {'range_name': 'marker', 'content': 'a1_name', 'sanitise': False, 'default_value': ''},
                        ),
                        'Supervisor': (
                            {'range_name': 'mark_thesis', 'content': 'mark_thesis', 'sanitise': False,
                             'default_value': 0},
                            {'range_name': 'mark_performance', 'content': 'mark_performance', 'sanitise': False,
                             'default_value': 0},
                        )
                    },
                'supervisor_summary_sections': (),
                'summary_sheet_columns':
                    (
                        {'title': 'Assessor 1', 'content': 'a1_name', 'sanitise': False, 'width': 20,
                         'default_value': 'Not returned yet'},
                        {'title': 'Performance', 'content': 'mark_performance', 'sanitise': False, 'width': 15,
                         'default_value': 'NULL'},
                        {'title': 'Supervisor Thesis', 'content': 'mark_thesis', 'sanitise': False, 'width': 15,
                         'default_value': 'NULL'},
                        {'title': 'Assessor Thesis', 'content': 'a1_mark', 'sanitise': False, 'width': 15,
                         'default_value': 'NULL'},
                    )
            },
            'ers-ipp': {
                'mark_columns':
                    (
                        {'column_name': 'IPP technical mark (70%) [Total Pts: 0 Score] |779720', 'content': 'mark_final',
                         'default_value': 0},
                    ),
                'feedback_columns':
                    (
                        {'column_name': 'IPP Feedback [Total Pts: 0 Text] |773380', 'content': 'a1_feedback',
                         'default_value': ''},
                    ),
                'extract_named_ranges':
                    {
                        'Assessor 1': (
                            {'range_name': 'mark_final', 'content': 'mark_final', 'sanitise': False,
                             'default_value': 0},
                            {'range_name': 'marker', 'content': 'a1_name', 'sanitise': False, 'default_value': ''},
                            {'range_name': 'feedback', 'content': 'a1_feedback', 'sanitise': True,
                             'default_value': []},
                        ),
                    },
                'supervisor_summary_sections':
                    (
                        {'title': 'Project Title', 'content': 'project_title', 'list': False},
                        {'title': 'Technical Mark', 'content': 'mark_final', 'list': False},
                        {'title': 'Feedback', 'content': 'a1_feedback', 'list': True,
                         'default_value': ['Not returned yet']},
                    ),
                'summary_sheet_columns':
                    (
                        {'title': 'Assessor', 'content': 'a1_name', 'sanitise': False, 'width': 20,
                         'default_value': 'Not returned yet'},
                        {'title': 'Technical Mark', 'content': 'mark_final', 'sanitise': False, 'width': 10,
                         'default_value': 'NULL'},
                        {'title': 'Feedback', 'content': 'a1_feedback', 'sanitise': True, 'width': 100,
                         'default_value': 'Not returned yet'},
                    )
            },
            'ers-rrr': {
                'mark_columns':
                    (
                        {'column_name': 'Engineering Research Skills Research Review [Total Pts: 100 Score] |753428',
                         'content': 'mark_final', 'default_value': 0},
                    ),
                'feedback_columns':
                    (
                        {'column_name': 'RRR feedback [Total Pts: 0 Text] |794805', 'content': 'feedback',
                         'default_value': ''},
                    ),
                'extract_named_ranges':
                    {
                        'Supervisor': (
                            {'range_name': 'mark_final', 'content': 'mark_final', 'sanitise': False,
                             'default_value': 0},
                            {'range_name': 'marker', 'content': 'marker', 'sanitise': False, 'default_value': ''},
                            {'range_name': 'feedback', 'content': 'feedback', 'sanitise': True,
                             'default_value': []},
                            {'range_name': 'staff_notes', 'content': 'staff_notes', 'sanitise': True,
                             'default_value': []},
                        ),
                    },
                'supervisor_summary_sections':
                    (
                        {'title': 'Project Title', 'content': 'project_title', 'list': False},
                        {'title': 'RRR Mark', 'content': 'mark_final', 'list': False},
                        {'title': 'Feedback', 'content': 'feedback', 'list': True,
                         'default_value': ['Not returned yet']},
                        {'title': 'Staff Notes', 'content': 'staff_notes', 'list': True,
                         'default_value': ['Not returned yet']},
                    ),
                'summary_sheet_columns':
                    (
                        {'title': 'Marker', 'content': 'marker', 'sanitise': False, 'width': 20,
                         'default_value': 'Not returned yet'},
                        {'title': 'RRR Mark', 'content': 'mark_final', 'sanitise': False, 'width': 10,
                         'default_value': 'NULL'},
                        {'title': 'Feedback', 'content': 'feedback', 'sanitise': True, 'width': 100,
                         'default_value': 'Not returned yet'},
                        {'title': 'Staff Notes', 'content': 'staff_notes', 'sanitise': True, 'width': 100,
                         'default_value': 'Not returned yet'},
                    )
            },
            'pgt-poster': {
                'mark_columns':
                    (
                        {'column_name': 'A1 Poster [Total Pts: 100 Score] |724313', 'content': 'a1_mark',
                         'default_value': 0},
                        {'column_name': 'A2 Poster [Total Pts: 100 Score] |724314', 'content': 'supervisor_mark',
                         'default_value': 0},
                    ),
                'feedback_columns':
                    (
                        {'column_name': 'A1 Poster Feedback [Total Pts: 0 Text] |724315', 'content': 'a1_feedback',
                         'default_value': ''},
                        {'column_name': 'A2 Poster Feedback [Total Pts: 0 Text] |724316', 'content': 'supervisor_feedback',
                         'default_value': ''},
                    ),
                'extract_named_ranges':
                    {
                        'Assessor 1': (
                            {'range_name': 'mark_final', 'content': 'a1_mark', 'sanitise': False,
                             'default_value': 0},
                            {'range_name': 'marker', 'content': 'a1_name', 'sanitise': False, 'default_value': ''},
                            {'range_name': 'feedback', 'content': 'a1_feedback', 'sanitise': True,
                             'default_value': []},
                        ),
                        'Supervisor': (
                            {'range_name': 'mark_final', 'content': 'supervisor_mark', 'sanitise': False,
                             'default_value': 0},
                            {'range_name': 'marker', 'content': 'supervisor_name', 'sanitise': False, 'default_value': ''},
                            {'range_name': 'feedback', 'content': 'supervisor_feedback', 'sanitise': True,
                             'default_value': []},
                        ),
                    },
                'supervisor_summary_sections':
                    (
                        {'title': 'Project Title', 'content': 'project_title', 'list': False},
                        {'title': 'A1 Feedback', 'content': 'a1_feedback', 'list': True,
                         'default_value': ['Not returned yet']},
                        {'title': 'Supervisor Feedback', 'content': 'supervisor_feedback', 'list': True,
                         'default_value': ['Not returned yet']},
                    ),
                'summary_sheet_columns':
                    (
                        {'title': 'Assessor 1', 'content': 'a1_name', 'sanitise': False, 'width': 20,
                         'default_value': 'Not returned yet'},
                        {'title': 'A1 Poster', 'content': 'a1_mark', 'sanitise': False, 'width': 10,
                         'default_value': 'NULL'},
                        {'title': 'S Poster', 'content': 'supervisor_mark', 'sanitise': False, 'width': 10,
                         'default_value': 'NULL'},
                        {'title': 'A1 Feedback', 'content': 'a1_feedback', 'sanitise': True, 'width': 100,
                         'default_value': 'Not returned yet'},
                        {'title': 'S Feedback', 'content': 'supervisor_feedback', 'sanitise': True, 'width': 100,
                         'default_value': 'Not returned yet'},
                    )
            },
            'pgt-thesis': {
                'mark_columns':
                    (
                        {'column_name': 'Supervisor Performance', 'content': 'mark_performance',
                         'default_value': 0},
                        {'column_name': 'Supervisor Thesis', 'content': 'mark_thesis',
                         'default_value': 0},
                        {'column_name': 'Assessor Thesis', 'content': 'a1_mark',
                         'default_value': 0},
                    ),
                'feedback_columns': (),
                'extract_named_ranges':
                    {
                        'Assessor 1': (
                            {'range_name': 'mark_final', 'content': 'a1_mark', 'sanitise': False, 'default_value': 0},
                            {'range_name': 'marker', 'content': 'a1_name', 'sanitise': False, 'default_value': ''},
                        ),
                        'Supervisor': (
                            {'range_name': 'mark_thesis', 'content': 'mark_thesis', 'sanitise': False,
                             'default_value': 0},
                            {'range_name': 'mark_performance', 'content': 'mark_performance', 'sanitise': False,
                             'default_value': 0},
                        )
                    },
                'supervisor_summary_sections': (),
                'summary_sheet_columns':
                    (
                        {'title': 'Assessor 1', 'content': 'a1_name', 'sanitise': False, 'width': 20,
                         'default_value': 'Not returned yet'},
                        {'title': 'Performance', 'content': 'mark_performance', 'sanitise': False, 'width': 15,
                         'default_value': 'NULL'},
                        {'title': 'Supervisor Thesis', 'content': 'mark_thesis', 'sanitise': False, 'width': 15,
                         'default_value': 'NULL'},
                        {'title': 'Assessor Thesis', 'content': 'a1_mark', 'sanitise': False, 'width': 15,
                         'default_value': 'NULL'},
                    )
            },
        }

        # load file, find username and mark and add to projects
        self.__scan_input_dir__()

        feedback_columns = extractors[EEEProjects.args.stage[0]]['feedback_columns']
        mark_columns = extractors[EEEProjects.args.stage[0]]['mark_columns']
        extract_named_ranges = extractors[EEEProjects.args.stage[0]]['extract_named_ranges']

        for file_name in self.files_list:
            self.__extract_named_ranges__(file_name, extract_named_ranges)

        self.__dump_marks_and_feedback_to_csv__(feedback_columns, mark_columns)

        summary_sheet_columns = extractors[EEEProjects.args.stage[0]]['summary_sheet_columns']
        self.__create_summary_sheet__(summary_sheet_columns)

        supervisor_summary_sections = extractors[EEEProjects.args.stage[0]]['supervisor_summary_sections']
        if supervisor_summary_sections:
            self.__create_supervisor_summary__(supervisor_summary_sections)


if __name__ == '__main__':
    start_time = time.time()

    # Initialise the top-level class
    EEEProjects.init()

    print('Operating in mode "%s"' % (EEEProjects.args.mode,))

    # Create output directory
    EEEProjects.create_out_dir()

    # Branch off based on --mode
    if EEEProjects.args.mode == 'generate':
        # Collect project list from input CSV
        EEEProjects.parse_project_list()

        logging.info("Done reading project list")

        if logging.root.level == logging.DEBUG:
            EEEProjects.staff.sort()
        logging.debug(EEEProjects.staff)

        FormGenerator(EEEProjects.projects).generate()

    if EEEProjects.args.mode != 'generate':
        MarkExtractor().extract()

    print('Done in %s seconds!' % (time.time() - start_time,))
